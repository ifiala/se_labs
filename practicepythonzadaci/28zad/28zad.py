def maxOfThree(a, b, c):                                                        
    maxValue = a                                                                
                                                                                
    if b > maxValue:                                                            
        maxValue = b                                                            
                                                                                
    if c > maxValue:                                                            
        maxValue = c                                                            
                                                                                
    return maxValue                                                             
                                                                                
if __name__ == "__main__":                                                      
    inputStrs = input("Enter 3 numbers: ").split(' ')                           
    values = [int(c) for c in inputStrs]                                        
    print(maxOfThree(values[0], values[1], values[2]))