import random

#Function that choose the word from the .txt
def ChooseWord(file):
    with open(file,"r") as f:
        lines=f.readlines()
    lines=[i.replace("\n","") for i in lines]
    return random.sample(lines,1)

#Function that ask you for a letter and check if its correct
#+ count incorrect tries

def AsknCheck(word,guessword,used_letter):
    while True:

        letter=input("Guess your letter:")
        letter=letter.upper()
        if letter in used_letter:
            print("You guessed this letter already.")

        elif letter in word:
            print("Guess letter: " +letter)
            used_letter.add(letter)
            return letter
            break
        else:
            print("Incorrect!")
            used_letter.add(letter)
            return 0


#Function that complements the word when you guessed correctly.
def WriteWord(guessword,word,letter):
    for i in range(len(word)):
        if word[i]==letter:
            guessword[i]=letter
        elif guessword[i]!="-":
            guessword[i]=guessword[i]
        else:
            guessword[i]="-"
    return guessword

#Function that is creating "-----" from the word at the very beginning.
def Initialization(word):
    startword=[]
    for i in range(len(word)):
        startword.append("-")
    return startword

#Function for drawing hangman.
def DrawHANGMAN(count):
    top="--------\n|      |\n|      |"
    mid={0: '|',
     1: "|      O",
     2: "|      O\n|     /",
     3: "|      O\n|     /|",
     4: "|      O\n|     /|\\",
     5: "|      O\n|     /|\\\n|     /",
     6: "|      O\n|     /|\\\n|     / \\"}
    mid1="|\n|\n|\n|"
    bottom="---"

    print(top)
    print(mid[count])
    print(mid1)
    print(bottom)



if __name__ == '__main__':
    ans=""
    while ans!="quit":
        word="".join(ChooseWord("sowpods.txt")) ## Random word from the .txt
        guessword=Initialization(word)
        used_letter=set()
        print("Welcome to HANGMAN. Please guess letter.")
        count=0
        DrawHANGMAN(count)
        while True:
            letter=AsknCheck(word,guessword,used_letter)
            if letter == 0:
                count+=1
                left=6-count
                DrawHANGMAN(count)
                print("You have " +str(left) +" incorrect guesses left")
            guessword=WriteWord(guessword,word,letter)
            if "".join(guessword)==word:
                print("You won't be hanged today.")
                break
            elif count==6:
                print("You have been hanged!")
                break
            print("".join(guessword))
        ans=input("If you want to end type:'quit'.")