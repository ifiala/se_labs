def draw_tic_tac_toe():
    game = [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]]
    play_player2 = 1
    play_player1 = 1
    while sum([(zeros.count(0)) for zeros in game]) != 0:
        if(play_player1):
            c_p_1 = input("Player1: Where do you want to place your X?")
            row1, col1 = int(c_p_1[0]), int(c_p_1[-1])
            if(row1 < len(game) and col1 < len(game)):
                if(game[row1][col1] != 'X' and game[row1][col1] != 'O'):
                    play_player1 = 1
                    play_player2 = 1
                    game[row1][col1] = 'X'
                    print(game)
                else:
                    print("Position 1 not allowed")
                    play_player2 = 0
                    continue
            else:
                print("Coords out of bounds")
                continue
        if(play_player2):
            c_p_2 = input("Player2: Where do you want to place your O?")
            row2, col2 = int(c_p_2[0]), int(c_p_2[-1])
            if(row2 < len(game) and col2 < len(game)):
                if(play_player2):
                    if(game[row2][col2] != 'X' and game[row2][col2] != 'O'):
                        play_player2 = 1
                        play_player1 = 1
                        game[row2][col2] = 'O'
                        print(game)
                    else:
                        print("Position 2 not allowed")
                        play_player1 = 0
                        continue        
            else:
                print("Coords out of bounds")
                continue